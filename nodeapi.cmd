@echo off
IF "%~1"=="" (
	GOTO BLANK
)ELSE IF "%~1"=="generate" (
	IF "%~2"=="route" (
		GOTO generateRoute
	)
) ELSE (
	GOTO NotBlank
)
:NotBlank 
echo Generating a Node API directory for %1
npm install -g sequelize-cli express-generator && ^
express --view=pug %1 && cd %1 && ^
npm install && ^
npm i --save sequelize mysql2 && ^
sequelize init && ^
sed -i 's/database_/%1_/g' config/config.json && ^
sed -i 's/"root"/"%1_user"/g' config/config.json && ^
sed -i 's/"password": null/"password": "apipassword"/g' config/config.json && ^
sed -i 's/"127.0.0.1"/"localhost"/g' config/config.json && ^
cp config/config.json config/config.example.json && ^
echo //put this above the app export ---- app.listen(3001); >> app.js && ^
sed -i 's/\//\\\\/' models/index.js && ^
touch migrations/.gitkeep && ^
touch models/.gitkeep && ^
touch seeders/.gitkeep && ^
touch public/images/.gitkeep && ^
touch public/javascripts/.gitkeep && ^
touch public/stylesheets/.gitkeep && ^
mkdir .nodeapicmd && ^
attrib +h .nodeapicmd && ^
cd .nodeapicmd && ^
mkdir node-api-templates && ^
cd .. && ^
cp ../nodeapi.cmd ./ && ^
cp -a ../node-api-templates/ .nodeapicmd/node-api-templates/ && ^
git init && ^
echo node_modules/ >> .gitignore && ^
echo config/config.json >> .gitignore && ^
git add . && ^
git commit -m "Initial Commit - %1 - Project Created" && ^
curl -H "Content-Type:application/json" https://gitlab.com/api/v4/projects?private_token=js7DimqGJrc9ed8Rkq5L -d "{ \"name\": \"%1\" }" && ^
git remote add origin git@gitlab.com:quincarter/%1.git && ^
git push -u origin master && ^
git flow init && ^
git push -u origin develop

SET var="success"

if "var"=="success" (
	GOTO finalMessage
)
break;
:finalMessage
	echo FINAL NOTES. IMPORTANT!!!!!
	echo ____________________________________________________________
	echo Sequelize Commands
	echo ____________________________________________________________
	echo --creating a model/migration for that model
	echo sequelize model:create --name Todo --attributes title:string,description:string
	echo ____________________________________________________________
	echo --executing a migration
	echo sequelize db:migrate
	echo ____________________________________________________________
	echo --undoing the last migration or all
	echo sequelize db:migrate:undo
	echo sequelize db:migrate:undo:all
	echo ____________________________________________________________
	echo SUCCESS!!!!
	echo A git repository has been initialized and all the files have been added/committed.
	echo be sure to add a git remote to get the project up online!
	break;
:BLANK
	echo No Project Name was given!
	break
:generateRoute
	IF "%~3"=="" (
		echo Destination project name was not given!
		break
	) ELSE (
		IF "%~4"=="" (
			echo No Route name was defined!
			break
		) ELSE (
			cp "E:\Documents\node-api-templates\route.js" "E:\Documents\%3\routes\%4.js" && cd "E:\Documents\%3\routes" && sed -i 's/templateName/%4/g' %4.js && cd "E:\Documents"
			echo Route Generated for %3/routes/%4.js
			break
		)
	)
	break;