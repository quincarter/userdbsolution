var express = require('express');
var router = express.Router();
var model = require('../models/index');
 
/* GET templateName listing. */
router.get('/', function(req, res, next) {
 
});
 
/* POST templateName. */
router.post('/', function(req, res, next) {
 
});
 
/* UPDATE templateName. */
router.put('/:id', function(req, res, next) {
 
});
 
/* DELETE templateName. */
router.delete('/:id', function(req, res, next) {
 
});

module.exports = router;