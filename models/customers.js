'use strict';
module.exports = (sequelize, DataTypes) => {
  var customers = sequelize.define('customers', {
    name: DataTypes.STRING
  }, {
    underscored: true,
  });
  customers.associate = function(models) {
    // associations can be defined here
  };
  return customers;
};