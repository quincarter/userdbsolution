'use strict';
module.exports = (sequelize, DataTypes) => {
  var customer_addresses = sequelize.define('customer_addresses', {
    customer_id: DataTypes.INTEGER,
    street_address: DataTypes.STRING,
    postal_code: DataTypes.INTEGER,
    country: DataTypes.STRING
  }, {
    underscored: true,
  });
  customer_addresses.associate = function(models) {
    // associations can be defined here
  };
  return customer_addresses;
};