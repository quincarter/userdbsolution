var express = require('express');
var router = express.Router();
var model = require('../models/customer_addresses');

/* GET customerAddresses listing. */
router.get('/customerAddresses', function(req, res, next) {
    model.customer_addresses.findAll({})
        .then(customer_addresses => res.json({
        error: false,
        data: customerAddresses
    }))
.catch(error => res.json({
        error: true,
        data: [],
        error: error
    })
)
});

/* POST customerAddresses. */
router.post('/customerAddresses', function(req, res, next) {

});

/* UPDATE customerAddresses. */
router.put('/customerAddresses/:id', function(req, res, next) {

});

/* DELETE customerAddresses. */
router.delete('/customerAddresses/:id', function(req, res, next) {

});

module.exports = router;
