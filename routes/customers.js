var express = require('express');
var router = express.Router();
var model = require('../models/index');
 
/* GET customers listing. */
router.get('/customers', function(req, res, next) {
 
});
 
/* POST customers. */
router.post('/customers', function(req, res, next) {
 
});
 
/* UPDATE customers. */
router.put('/customers/:id', function(req, res, next) {
 
});
 
/* DELETE customers. */
router.delete('/customers/:id', function(req, res, next) {
 
});

module.exports = router;